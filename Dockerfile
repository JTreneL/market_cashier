FROM fedora
RUN dnf install -y python3-pip wget
RUN pip install selenium
RUN dnf install -y firefox
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
RUN sudo tar -xvf geckodriver-v0.30.0-linux64.tar.gz
RUN sudo mv geckodriver usr/bin/
RUN cd usr/bin/ && sudo chmod +x geckodriver
RUN mkdir home/marketcashier
WORKDIR home/marketcashier
COPY . .
ENTRYPOINT ["python3", "Market€ashier.py"]
