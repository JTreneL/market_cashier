import time
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
import os 
options = webdriver.FirefoxOptions()
options.headless = True 
driver = webdriver.Firefox(options=options)


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


def gold():
    comodity = "GOLD"
    print("COMODITY GOLD IS SELECTED.")
    driver.get('https://www.kurzy.cz/komodity/zlato-graf-vyvoje-ceny/1oz-usd-1-den')
    onlineprice = driver.find_element(By.XPATH,'//*[@id="last_czk"]').text
    onlineprice = float(onlineprice)/ 31.103
    menu2(comodity,onlineprice)


def silver():
    comodity = "SILVER"
    print("COMODITY SILVER IS SELECTED.")
    driver.get('https://www.kurzy.cz/komodity/stribro-graf-vyvoje-ceny/')
    onlineprice = driver.find_element(By.XPATH,'//*[@id="last_czk"]').text
    onlineprice = float(onlineprice)/ 31.103
    menu2(comodity,onlineprice)


def palladium():
    comodity = "PALLADIUM"
    print("COMODITY PALADIUM IS SELECTED.")
    driver.get('https://www.kurzy.cz/komodity/palladium-graf-vyvoje-ceny/')
    time.sleep(3)
    onlineprice = driver.find_element(By.XPATH,'//*[@id="last_czk"]').text
    onlineprice = float(onlineprice)/ 31.103
    menu2(comodity,onlineprice)


def platinum():
    comodity = "PLATINUM"
    print("COMODITY PLATINUM IS SELECTED.")
    driver.get('https://www.kurzy.cz/komodity/platina-graf-vyvoje-ceny/')
    onlineprice = driver.find_element(By.XPATH,'//*[@id="last_czk"]').text
    onlineprice = float(onlineprice)/ 31.103
    menu2(comodity,onlineprice)


def main():
    while True:
        print('COMODITY CASHIER FOR ONLINE BUYING.')
        menu1 = input("""SELECT COMODITY YOU WANT TO ADD
A. GOLD B. SILVER C. PALADIUM D. PLATINUM E.EXIT: """)
        if menu1 == "A":
            gold()
        elif menu1 == "B":
            silver()
        elif menu1 == "C":
           palladium()
        elif menu1 == "D":
            platinum()
        elif menu1 == "E":
            break
        else:
            print("WRONG COMMAND.")


def menu2(comodity,onlineprice):
    weight = input("ENTER WEIGHT IN GRAMS:")
    try:
        weight = int(weight)
        print("COMODITY:",comodity,"WEIGHT IN G:",weight,"ONLINE PRICE FOR 1 GRAM:",onlineprice)
        total = int(weight) * float(onlineprice)
        total = round(total, 1)
        print("TOTAL PRICE FOR:",comodity," IS ",total,"CZK.")
        time.sleep(5.5)
        cls()
        main()
    except ValueError:
        try:
            weigt = float(weight)
            print("COMODITY:",comodity,"WEIGHT IN G:",weight,"ONLINE PRICE FOR 1 GRAM:",onlineprice)
            total = int(weight) * float(onlineprice)
            total = round(total, 1)
            print("TOTAL PRICE FOR:",comodity," IS ",total,"CZK.")
            time.sleep(5.5)
            cls()
            main()
        except ValueError:
            print("INPUT IS NOT NUMBER, TRY IT AGAIN.")
            time.sleep(3)
            cls()


if __name__ == "__main__":
    sys.exit(main())